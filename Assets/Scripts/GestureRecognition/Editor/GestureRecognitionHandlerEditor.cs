﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GestureRecognitionHandler))]
public class GestureRecognitionHandlerEditor : Editor {

	private GestureRecognitionHandler m_SRHReference;

	private int m_IndentLevel = 1;
	private int m_NumberOfShape;

	private void PreProcess(){

		m_SRHReference = (GestureRecognitionHandler)target;
	}

	private void OnEnabled(){

		PreProcess ();
	}

	public override void OnInspectorGUI(){

		PreProcess ();

		serializedObject.Update ();

		ConfiguretionGUI();
		PublicVariableFieldGUI ();
		ShowShapeGUI ();

		serializedObject.ApplyModifiedProperties ();
	}

	private void ConfiguretionGUI(){

		GUILayout.Space (5.0f);
		EditorGUILayout.BeginHorizontal();{
			
			EditorGUILayout.PropertyField (
			serializedObject.FindProperty ("EnableSingleton"));
			EditorGUILayout.PropertyField (
			serializedObject.FindProperty ("EnableDebugMode"));
		}EditorGUILayout.EndHorizontal();

		if(m_SRHReference.EnableDebugMode){
			
			EditorGUILayout.PropertyField (
			serializedObject.FindProperty ("DisplayTime"));
			EditorGUILayout.PropertyField (
			serializedObject.FindProperty ("WhiteSprite"));
		}
	}

	private void PublicVariableFieldGUI(){
	
		GUILayout.Space (5.0f);
		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));
		EditorGUILayout.PropertyField (
			serializedObject.FindProperty ("overallSmoothness"));
	}

	private void ShowShapeGUI(){

		GUILayout.Space (5.0f);
		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));

		GUILayout.Space (5.0f);
		EditorGUILayout.BeginHorizontal ();{

			m_NumberOfShape = EditorGUILayout.IntField (
				"Shape",
				m_NumberOfShape);
			if (GUILayout.Button ("Create Shape")) {

				m_SRHReference.shape = new GestureRecognitionHandler.Shape[m_NumberOfShape];

			}
		}EditorGUILayout.EndHorizontal ();

		if (m_SRHReference.shape != null) {
		
			for (int shapeIndex = 0; shapeIndex < m_SRHReference.shape.Length; shapeIndex++) {
			
				GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));
				if (!m_SRHReference.m_EditorLock && (m_SRHReference.shape [shapeIndex].shapeMatrix == null
				|| m_SRHReference.shape [shapeIndex].shapeMatrix.Length != Mathf.Pow(m_SRHReference.shape [shapeIndex].shapeMatrixSize,2))) {

					int m_ShapeMatrixSize = m_SRHReference.shape [shapeIndex].shapeMatrixSize;
					m_SRHReference.shape [shapeIndex].shapeMatrix 	= new bool[m_ShapeMatrixSize * m_ShapeMatrixSize];
					m_SRHReference.shape [shapeIndex].numberOfRow 	= m_ShapeMatrixSize;
					m_SRHReference.shape [shapeIndex].numberOfColumn= m_ShapeMatrixSize;
				}

				EditorGUI.indentLevel += m_IndentLevel;
				m_SRHReference.shape[shapeIndex].isShowOnEditor = EditorGUILayout.Foldout (
					m_SRHReference.shape[shapeIndex].isShowOnEditor,
					"Shape (" + shapeIndex + ")");

				if (m_SRHReference.shape [shapeIndex].isShowOnEditor) {
				
					EditorGUI.indentLevel += m_IndentLevel;

					EditorGUILayout.PropertyField (
						serializedObject.FindProperty ("shape").
						GetArrayElementAtIndex (shapeIndex).
						FindPropertyRelative ("smoothness"));
					EditorGUILayout.PropertyField (
						serializedObject.FindProperty ("shape").
						GetArrayElementAtIndex (shapeIndex).
						FindPropertyRelative ("shapeMatrixSize"));

					EditorGUILayout.BeginVertical ();{
						
						int m_MatrixIndex = 0;
						for (int row = 0; row < m_SRHReference.shape [shapeIndex].numberOfRow; row++) {

							EditorGUILayout.BeginHorizontal ();{

								for (int column = 0; column < m_SRHReference.shape [shapeIndex].numberOfColumn; column++) {
									
									m_SRHReference.shape [shapeIndex].shapeMatrix [m_MatrixIndex] = EditorGUILayout.Toggle (
										m_SRHReference.shape [shapeIndex].shapeMatrix [m_MatrixIndex]);
									
									m_MatrixIndex++;
								}
							}EditorGUILayout.EndHorizontal ();
						}

						EditorGUILayout.PropertyField (
							serializedObject.FindProperty ("shape").
							GetArrayElementAtIndex (shapeIndex).
							FindPropertyRelative ("OnShapeFound"));
						EditorGUILayout.PropertyField (
							serializedObject.FindProperty ("shape").
							GetArrayElementAtIndex (shapeIndex).
							FindPropertyRelative ("OnShapeSearchBegin"));
						EditorGUILayout.PropertyField (
							serializedObject.FindProperty ("shape").
							GetArrayElementAtIndex (shapeIndex).
							FindPropertyRelative ("OnShapeSearchFailed"));

					}EditorGUILayout.EndVertical ();

					EditorGUI.indentLevel -= m_IndentLevel;
				}

				EditorGUI.indentLevel -= m_IndentLevel;
				GUILayout.Space (5.0f);
			}
		}
	}
}
