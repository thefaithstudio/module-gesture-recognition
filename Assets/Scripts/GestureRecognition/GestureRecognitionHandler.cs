﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GestureRecognitionHandler : MonoBehaviour {

	public static GestureRecognitionHandler Instance;

	#region Custom DataType

	public struct GridIdentifier {
		public bool isShapeGrid;
		public float minInterpolateX;
		public float maxInterpolateX;
		public float minInterpolateY;
		public float maxInterpolateY;
	}

	[System.Serializable]
	public struct Shape {
		[RangeAttribute (0.0f, 1.0f)]
		public float smoothness;
		[RangeAttribute (3, 10)]
		public int shapeMatrixSize;
		public bool[] shapeMatrix;

		public UnityEvent OnShapeFound;

		public UnityEvent OnShapeSearchBegin;
		public UnityEvent OnShapeSearchFailed;

		[HideInInspector]
		public bool isShowOnEditor;
		[HideInInspector]
		public int numberOfRow;
		[HideInInspector]
		public int numberOfColumn;
	}

	#endregion
	//----------
	#region Public Variables

	[HideInInspector]
	public bool EnableSingleton;
	[HideInInspector]
	public bool EnableDebugMode;
	public Sprite WhiteSprite;
	[Range(2.0f,10.0f)]
	public float DisplayTime;

	[HideInInspector]
	public bool m_EditorLock;
	[Range (0.0f, 1.0f)]
	public float overallSmoothness;
	public Shape[] shape;

	#endregion

	#region Private Variables

	private GameObject m_InputCamera;
	private Camera m_CameraReference;

	private List<Vector2> m_OnScreenPositionList;
	private bool m_IsTrackingUserInput;
	private int m_ShapeIndex;

	//Reference Debug Panel
	private GameObject m_ShapeIndicator,m_CorrectPointOnShape,m_IncorrectPointOnShape;
	private List<GameObject> m_ListOfShapeIndicator,m_ListOfCorrectedShapeIndicator,m_ListOfIncorrectedShapeIndicator;

	#endregion

	//----------
	#region Mono Behaviour Function

	void Awake () {

		if (EnableSingleton) {
			if (Instance == null) {

				Instance = this;

			} else if (Instance != this) {

				Destroy (gameObject);
			}
		}

		PreProcess ();
		OptimizedShape ();
		m_EditorLock = true;

	}

	private void Start () {

		FindShape(0);
	}

	private void Update () {

		if (m_IsTrackingUserInput) {

			UserInput ();
		}
	}

	#endregion

	//----------
	#region Public Callback

	public void FindShape (int m_ShapeIndex) {

		this.m_ShapeIndex = m_ShapeIndex;
		m_IsTrackingUserInput = true;
	}

	#endregion

	//----------
	#region Configuretion

	private void PreProcess () {

		m_InputCamera = new GameObject ();
		m_InputCamera.transform.parent = transform;
		m_InputCamera.name = "InputCamera";

		m_InputCamera.AddComponent<Camera> ();
		m_InputCamera.GetComponent<Camera> ().clearFlags = CameraClearFlags.Nothing;
		m_InputCamera.GetComponent<Camera> ().cullingMask = 0;
		m_InputCamera.GetComponent<Camera> ().orthographic = true;
		m_InputCamera.GetComponent<Camera> ().allowHDR = false;
		m_InputCamera.GetComponent<Camera> ().allowMSAA = false;
		m_InputCamera.GetComponent<Camera> ().useOcclusionCulling = false;

		m_CameraReference = m_InputCamera.GetComponent<Camera> ();

		if(EnableDebugMode)
			DebugModeInitializer();
		}

	// Start : Debug Configuretion
	private void DebugModeInitializer(){

		m_ShapeIndicator = new GameObject();
		m_ShapeIndicator.transform.parent = transform;
		m_ShapeIndicator.transform.localPosition = Vector3.zero;
		m_ShapeIndicator.name = "ShapeIndicator";
		m_ShapeIndicator.AddComponent<SpriteRenderer>();
		SpriteRenderer m_SRReference = m_ShapeIndicator.GetComponent<SpriteRenderer>();
		m_SRReference.sprite = WhiteSprite;
		m_SRReference.color = Color.blue;
		m_ShapeIndicator.SetActive(false);
		m_ListOfShapeIndicator = new List<GameObject>();

		m_CorrectPointOnShape = new GameObject();
		m_CorrectPointOnShape.transform.parent = transform;
		m_CorrectPointOnShape.transform.localPosition = Vector3.zero;
		m_CorrectPointOnShape.name = "CorrectShapeIndicator";
		m_CorrectPointOnShape.AddComponent<SpriteRenderer>();
		SpriteRenderer m_CPOSReference = m_CorrectPointOnShape.GetComponent<SpriteRenderer>();
		m_CPOSReference.sprite = WhiteSprite;
		m_CPOSReference.color = Color.green;
		m_CorrectPointOnShape.SetActive(false);
		m_ListOfCorrectedShapeIndicator = new List<GameObject>();

		m_IncorrectPointOnShape = new GameObject();
		m_IncorrectPointOnShape.transform.parent = transform;
		m_IncorrectPointOnShape.transform.localPosition = Vector3.zero;
		m_IncorrectPointOnShape.name = "IncorrectShapeIndicator";
		m_IncorrectPointOnShape.AddComponent<SpriteRenderer>();
		SpriteRenderer m_IPOSReference = m_IncorrectPointOnShape.GetComponent<SpriteRenderer>();
		m_IPOSReference.sprite = WhiteSprite;
		m_IPOSReference.color = Color.red;
		m_IncorrectPointOnShape.SetActive(false);
		m_ListOfIncorrectedShapeIndicator = new List<GameObject>();
	}

	private void CreateShapeIndicator(Vector2 m_Position){

		Vector3 m_ModifiedPosition = new Vector3(
			m_Position.x,
			m_Position.y,
			-1.0f
		);
		GameObject newShapeIndicatorObject = Instantiate(m_ShapeIndicator,Vector3.zero,Quaternion.identity) as GameObject;
		newShapeIndicatorObject.transform.parent = transform;
		newShapeIndicatorObject.transform.localPosition = m_ModifiedPosition;
		m_ListOfShapeIndicator.Add(newShapeIndicatorObject);
		if(!newShapeIndicatorObject.activeInHierarchy){
			newShapeIndicatorObject.SetActive(true);
		}
	}

	private void CreateCorrectShapeIndicator(Vector2 m_Position){
		
		GameObject newCorrectShapeIndicatorObject = Instantiate(m_CorrectPointOnShape,Vector3.zero,Quaternion.identity) as GameObject;
		newCorrectShapeIndicatorObject.transform.parent = transform;
		newCorrectShapeIndicatorObject.transform.localPosition = m_Position;
		m_ListOfCorrectedShapeIndicator.Add(newCorrectShapeIndicatorObject);
		if(!newCorrectShapeIndicatorObject.activeInHierarchy){
			newCorrectShapeIndicatorObject.SetActive(true);
		}
	}

	private void CreateIncorrectShapeIndicator(Vector2 m_Position){

		GameObject newInCorrectShapeIndicatorObject = Instantiate(m_IncorrectPointOnShape,Vector3.zero,Quaternion.identity) as GameObject;
		newInCorrectShapeIndicatorObject.transform.parent = transform;
		newInCorrectShapeIndicatorObject.transform.localPosition = m_Position;
		m_ListOfIncorrectedShapeIndicator.Add(newInCorrectShapeIndicatorObject);
		if(!newInCorrectShapeIndicatorObject.activeInHierarchy){
			newInCorrectShapeIndicatorObject.SetActive(true);
		}
	}

	private IEnumerator ResetConfiguretion(){

		yield return new WaitForSeconds(DisplayTime);

		for(int index = 0; index < m_ListOfShapeIndicator.Count ; index++)
			Destroy(m_ListOfShapeIndicator[index]);
		m_ListOfShapeIndicator.Clear();

		for(int index = 0; index < m_ListOfCorrectedShapeIndicator.Count ; index++)
			Destroy(m_ListOfCorrectedShapeIndicator[index]);
		m_ListOfCorrectedShapeIndicator.Clear();

		for(int index = 0; index < m_ListOfIncorrectedShapeIndicator.Count ; index++)
			Destroy(m_ListOfIncorrectedShapeIndicator[index]);
		m_ListOfIncorrectedShapeIndicator.Clear();
	}

	// End : Debug Configuretion
	private void OptimizedShape () {

		for (int shapeIndex = 0; shapeIndex < shape.Length; shapeIndex++) {

			int m_ShapeMatrixSize = shape[shapeIndex].shapeMatrixSize;
			bool[, ] m_ShapeMatrixReference = new bool[m_ShapeMatrixSize, m_ShapeMatrixSize];
			bool[] m_FilledRow = new bool[m_ShapeMatrixSize];
			bool[] m_FilledColumn = new bool[m_ShapeMatrixSize];

			int m_LoopBoundary = m_ShapeMatrixSize;
			int m_UpperBound = m_LoopBoundary;
			int m_LowerBound = 0;

			for (int row = 0, m_MatrixIndex = 0; row < m_ShapeMatrixSize; row++) {

				for (int column = 0; column < m_ShapeMatrixSize; column++) {

					m_ShapeMatrixReference[row, column] = shape[shapeIndex].shapeMatrix[m_MatrixIndex];
					m_MatrixIndex++;
				}
			}

			#region Optimizing Row Count

			for (int row = 0; row < m_ShapeMatrixSize; row++) {

				for (int column = 0; column < m_ShapeMatrixSize; column++) {

					if (m_ShapeMatrixReference[row, column]) {

						m_FilledRow[row] = true;
						break;
					}
				}
			}

			for (int upperBound = m_ShapeMatrixSize - 1; upperBound >= 0; upperBound--) {

				if (m_FilledRow[upperBound]) {

					m_UpperBound = upperBound;
					break;
				}
			}

			for (int lowerBound = 0; lowerBound < m_LoopBoundary; lowerBound++) {

				if (m_FilledRow[lowerBound]) {

					m_LowerBound = lowerBound;
					break;
				}
			}

			int m_ModifiedRowSize = Mathf.Abs (m_UpperBound - m_LowerBound) + 1;
			bool[, ] m_RowOptimizedGrid = new bool[m_ModifiedRowSize, m_ShapeMatrixSize];
			for (int row = 0, m_TrackIndex = m_LowerBound; row < m_ModifiedRowSize; row++, m_TrackIndex++) {

				for (int column = 0; column < m_ShapeMatrixSize; column++) {

					m_RowOptimizedGrid[row, column] = shape[shapeIndex].shapeMatrix[(m_TrackIndex * m_ShapeMatrixSize) + column];
				}
			}

			shape[shapeIndex].numberOfRow = m_ModifiedRowSize;
			shape[shapeIndex].numberOfColumn = m_ShapeMatrixSize;
			shape[shapeIndex].shapeMatrix = new bool[m_ModifiedRowSize * m_ShapeMatrixSize];
			int m_ModfiedRowIndex = 0;
			for (int row = 0; row < m_ModifiedRowSize; row++) {

				for (int column = 0; column < m_ShapeMatrixSize; column++) {

					shape[shapeIndex].shapeMatrix[m_ModfiedRowIndex] = m_RowOptimizedGrid[row, column];
					m_ModfiedRowIndex++;
				}
			}

			#endregion

			//----------
			#region Optimizing Column Count

			for (int column = 0; column < m_ShapeMatrixSize; column++) {

				for (int row = 0; row < m_ModifiedRowSize; row++) {

					if (m_RowOptimizedGrid[row, column]) {

						m_FilledColumn[column] = true;
						break;
					}
				}
			}

			for (int upperBound = m_ShapeMatrixSize - 1; upperBound >= 0; upperBound--) {

				if (m_FilledColumn[upperBound]) {

					m_UpperBound = upperBound;
					break;
				}
			}

			for (int lowerBound = 0; lowerBound < m_LoopBoundary; lowerBound++) {

				if (m_FilledColumn[lowerBound]) {

					m_LowerBound = lowerBound;
					break;
				}
			}

			int m_ModifiedColumnSize = Mathf.Abs (m_UpperBound - m_LowerBound) + 1;
			bool[, ] m_ColumnOptimizedGrid = new bool[m_ModifiedRowSize, m_ModifiedColumnSize];
			for (int row = 0; row < m_ModifiedRowSize; row++) {

				for (int column = 0; column < m_ModifiedColumnSize; column++) {

					m_ColumnOptimizedGrid[row, column] = m_RowOptimizedGrid[row, (m_LowerBound + column)];
				}
			}

			shape[shapeIndex].numberOfRow = m_ModifiedRowSize;
			shape[shapeIndex].numberOfColumn = m_ModifiedColumnSize;
			shape[shapeIndex].shapeMatrix = new bool[m_ModifiedRowSize * m_ModifiedColumnSize];
			int m_ModfiedColumnIndex = 0;
			for (int row = 0; row < m_ModifiedRowSize; row++) {

				for (int column = 0; column < m_ModifiedColumnSize; column++) {

					shape[shapeIndex].shapeMatrix[m_ModfiedColumnIndex] = m_ColumnOptimizedGrid[row, column];
					m_ModfiedColumnIndex++;
				}
			}
			#endregion
		}
	}

	private void UserInput () {

	#if UNITY_EDITOR

		if (Input.GetMouseButtonDown (0)) {

			Vector3 m_MousePosition = Input.mousePosition;
			OnKeyPressDown (m_CameraReference.ScreenToWorldPoint (new Vector3 (m_MousePosition.x, m_MousePosition.y, m_CameraReference.nearClipPlane)));
		}

		if (Input.GetMouseButton (0)) {

			Vector3 m_MousePosition = Input.mousePosition;
			OnKeyPress (m_CameraReference.ScreenToWorldPoint (new Vector3 (m_MousePosition.x, m_MousePosition.y, m_CameraReference.nearClipPlane)));
		}

		if (Input.GetMouseButtonUp (0)) {

			Vector3 m_MousePosition = Input.mousePosition;
			OnKeyPressUp (m_CameraReference.ScreenToWorldPoint (new Vector3 (m_MousePosition.x, m_MousePosition.y, m_CameraReference.nearClipPlane)));
		}

	#elif UNITY_ANDROID || UNITY_IOS

		if(Input.touchCount == 1){
			
			Touch m_UserTouch = Input.GetTouch(0);

			switch(m_UserTouch.phase){

				case TouchPhase.Began:
				OnKeyPressDown(m_CameraReference.ScreenToWorldPoint (new Vector3 (m_UserTouch.position.x, m_UserTouch.position.y, m_CameraReference.nearClipPlane)));
				break;
				case TouchPhase.Moved:
				OnKeyPress(m_CameraReference.ScreenToWorldPoint (new Vector3 (m_UserTouch.position.x, m_UserTouch.position.y, m_CameraReference.nearClipPlane)));
				break;
				case TouchPhase.Canceled:
				OnKeyPressUp(m_CameraReference.ScreenToWorldPoint (new Vector3 (m_UserTouch.position.x, m_UserTouch.position.y, m_CameraReference.nearClipPlane)));
				break;
				case TouchPhase.Ended:
				OnKeyPressUp(m_CameraReference.ScreenToWorldPoint (new Vector3 (m_UserTouch.position.x, m_UserTouch.position.y, m_CameraReference.nearClipPlane)));
				break;

			}	
		}
	#endif
	}

	private void OnKeyPressDown (Vector2 m_PressPosition) {

		//Debug.Log("OnKeyPressedDown : " + m_PressPosition);
		m_OnScreenPositionList = new List<Vector2> ();
		m_OnScreenPositionList.Add (m_PressPosition);
	}

	private void OnKeyPress (Vector2 m_PressPosition) {

		//Debug.Log("OnKeyPressed : " + m_PressPosition);

		bool m_IsPositionFound = false;

		for (int m_ListIndex = 0; m_ListIndex < m_OnScreenPositionList.Count; m_ListIndex++) {

			if (m_OnScreenPositionList[m_ListIndex] == m_PressPosition) {

				m_IsPositionFound = true;
				break;
			}
		}

		if (!m_IsPositionFound)
			m_OnScreenPositionList.Add (m_PressPosition);
	}

	private void OnKeyPressUp (Vector2 m_PressPosition) {

		//Debug.Log("OnKeyPressedUp : " + m_PressPosition);

		bool m_IsPositionFound = false;

		for (int m_ListIndex = 0; m_ListIndex < m_OnScreenPositionList.Count; m_ListIndex++) {

			if (m_OnScreenPositionList[m_ListIndex] == m_PressPosition) {

				m_IsPositionFound = true;
				break;
			}
		}

		if (!m_IsPositionFound)
			m_OnScreenPositionList.Add (m_PressPosition);

		m_IsTrackingUserInput = false;
		
		ShapeRecognition ();

		if(EnableDebugMode)
			StartCoroutine(ResetConfiguretion());
	}

	private void ShapeRecognition () {

		//Debug.Log("List Size : " + m_OnScreenPositionList.Count);

		float m_MaxX = 0;
		float m_MaxY = 0;
		float m_MinX = 0;
		float m_MinY = 0;

		for (int m_ListIndex = 0; m_ListIndex < m_OnScreenPositionList.Count; m_ListIndex++) {

			Vector2 m_Position = m_OnScreenPositionList[m_ListIndex];

			if (m_Position.x > m_MaxX) {

				m_MaxX = m_Position.x;
			} else if (m_Position.x < m_MinX) {

				m_MinX = m_Position.x;
			}

			if (m_Position.y > m_MaxY) {

				m_MaxY = m_Position.y;
			} else if (m_Position.y < m_MinY) {

				m_MinY = m_Position.y;
			}
		}

		//Begin	: Single Row/Column Correction
		Vector2 m_UserInputBegin 	= m_OnScreenPositionList[0];
		Vector2 m_UserInputEnd 		= m_OnScreenPositionList[m_OnScreenPositionList.Count-1];

		if(shape[m_ShapeIndex].numberOfRow == 1){
			
			if(m_UserInputEnd.y - m_UserInputBegin.y >= 0){
				
				//Direction : Up
				m_MinY = m_UserInputBegin.y;
				m_MaxY = m_MinY + (overallSmoothness * shape[m_ShapeIndex].smoothness);
			}else{
				
				//Direction : Down
				m_MinY = m_UserInputEnd.y;
				m_MaxY = m_MinY + (overallSmoothness * shape[m_ShapeIndex].smoothness);
			}
		}

		if(shape[m_ShapeIndex].numberOfColumn == 1){
			
			if(m_UserInputEnd.x - m_UserInputBegin.x >= 0){
				//Direction : Right
				m_MinX = m_UserInputBegin.x;
				m_MaxX = m_MinX + (overallSmoothness * shape[m_ShapeIndex].smoothness);
			}else{
				//Direction : Left
				m_MinX = m_UserInputEnd.x;
				m_MaxX = m_MinX + (overallSmoothness * shape[m_ShapeIndex].smoothness);
			}
		}
		//End 	: Single Row/Column Correction 	

		float m_MaxDifX = Mathf.Abs (m_MaxX - m_MinX);
		float m_MaxDifY = Mathf.Abs (m_MaxY - m_MinY);

		float m_InterpolateUnitX = m_MaxDifX / shape[m_ShapeIndex].numberOfColumn;
		float m_InterpolateUnitY = m_MaxDifY / shape[m_ShapeIndex].numberOfRow;

		//Debug.Log("Max : (" + m_MaxX + "," + m_MaxY + ")");
		//Debug.Log("Min : (" + m_MinX + "," + m_MinY + ")");
		//Debug.Log("DifX = " + m_MaxDifX + " : DifY = " + m_MaxDifY);
		//Debug.Log("InterpolateX : " + m_InterpolateUnitX + " , InterpolateY : " + m_InterpolateUnitY);

		GridIdentifier[] m_ShapeGridIndentifier = new GridIdentifier[shape[m_ShapeIndex].numberOfRow * shape[m_ShapeIndex].numberOfColumn];
		float m_InterpolateX = 0.0f;
		float m_InterpolateY = m_MaxDifY;
		for (int row = 0; row < shape[m_ShapeIndex].numberOfRow; row++) {

			for (int column = 0; column < shape[m_ShapeIndex].numberOfColumn; column++) {

				int m_Index = (row * shape[m_ShapeIndex].numberOfColumn) + column;

				if (shape[m_ShapeIndex].shapeMatrix[m_Index]) {

					m_ShapeGridIndentifier[m_Index].isShapeGrid = true;

					m_ShapeGridIndentifier[m_Index].minInterpolateX = m_InterpolateX / m_MaxDifX;
					m_ShapeGridIndentifier[m_Index].minInterpolateY = (m_InterpolateY - m_InterpolateUnitY) / m_MaxDifY;

					m_ShapeGridIndentifier[m_Index].maxInterpolateX = (m_InterpolateX + m_InterpolateUnitX) / m_MaxDifX;
					m_ShapeGridIndentifier[m_Index].maxInterpolateY = m_InterpolateY / m_MaxDifY;

					if(EnableDebugMode){
						float m_AbsoluteX = ((m_ShapeGridIndentifier[m_Index].minInterpolateX + m_ShapeGridIndentifier[m_Index].maxInterpolateX) / 2.0f) * m_MaxDifX;
						float m_AbsoluteY = ((m_ShapeGridIndentifier[m_Index].minInterpolateY + m_ShapeGridIndentifier[m_Index].maxInterpolateY) / 2.0f) * m_MaxDifY;
						CreateShapeIndicator(new Vector2(
							m_MinX + m_AbsoluteX,
							m_MinY + m_AbsoluteY
						));
					}

					//Debug.Log("(" + row + "," + column + ") Min Interpolate (" + m_ShapeGridIndentifier[m_Index].minInterpolateX + "," + m_ShapeGridIndentifier[m_Index].minInterpolateY + ") : Max Interpolate (" + m_ShapeGridIndentifier[m_Index].maxInterpolateX + "," + m_ShapeGridIndentifier[m_Index].maxInterpolateY + ")");
				} else {

					m_ShapeGridIndentifier[(row * shape[m_ShapeIndex].numberOfColumn) + column].isShapeGrid = false;
				}

				m_InterpolateX += m_InterpolateUnitX;

			}

			m_InterpolateY -= m_InterpolateUnitY;
			m_InterpolateX = 0.0f;
		}

		//Debug.Log("-----------");
		//Matching The Cordinate	
		int m_MatchCounter = 0;
		for (int inputCounter = 0; inputCounter < m_OnScreenPositionList.Count; inputCounter++) {

			float m_InterpolatedPositionX = Mathf.Abs (m_MinX - m_OnScreenPositionList[inputCounter].x) / m_MaxDifX;
			float m_InterpolatedPositionY = Mathf.Abs (m_MinY - m_OnScreenPositionList[inputCounter].y) / m_MaxDifY;
			//Debug.Log(inputCounter + ": (" + m_InterpolatedPositionX + "," + m_InterpolatedPositionY + ")");
			
			
			bool m_IsRightGrid = false;
			
			for (int gridIndex = 0; gridIndex < m_ShapeGridIndentifier.Length; gridIndex++) {

				if ((m_InterpolatedPositionX >= m_ShapeGridIndentifier[gridIndex].minInterpolateX && m_InterpolatedPositionX < m_ShapeGridIndentifier[gridIndex].maxInterpolateX) &&
					(m_InterpolatedPositionY >= m_ShapeGridIndentifier[gridIndex].minInterpolateY && m_InterpolatedPositionY < m_ShapeGridIndentifier[gridIndex].maxInterpolateY)) {

					//Debug.Log("Min : " + m_ShapeGridIndentifier[gridIndex].minInterpolateX + "," + m_ShapeGridIndentifier[gridIndex].minInterpolateY);
					//Debug.Log("Max : " + m_ShapeGridIndentifier[gridIndex].maxInterpolateX + "," + m_ShapeGridIndentifier[gridIndex].maxInterpolateY);
					//Debug.Log("MATCH (" + gridIndex + ") -> " + inputCounter + ": (" + m_InterpolatedPositionX + "," + m_InterpolatedPositionY + ")");
					m_MatchCounter++;

					if(EnableDebugMode){
						
						m_IsRightGrid = true;

						CreateCorrectShapeIndicator(new Vector2(
							m_OnScreenPositionList[inputCounter].x,
							m_OnScreenPositionList[inputCounter].y
						));
					}

					break;
				}
			}

			if(EnableDebugMode && !m_IsRightGrid){

					CreateIncorrectShapeIndicator(new Vector2(
							m_OnScreenPositionList[inputCounter].x,
							m_OnScreenPositionList[inputCounter].y
						));
				}
		}

		//Debug.Log("m_MatchCounter = " + m_MatchCounter);

		float m_Result = m_MatchCounter / (float) m_OnScreenPositionList.Count;
		float m_TargetedResult = 1.0f - (overallSmoothness * shape[m_ShapeIndex].smoothness);
		Debug.Log ("Recognition = " + (m_Result * 100.0f) + "% : Targeted Recognition = " + (m_TargetedResult * 100.0f) + "%");
		if (m_Result >= m_TargetedResult) {

			Debug.Log ("Recognized");
			shape[m_ShapeIndex].OnShapeFound.Invoke ();
		} else {

			Debug.Log ("Not Recognized");
			shape[m_ShapeIndex].OnShapeSearchFailed.Invoke ();
		}
	}

	#endregion
}