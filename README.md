# Documentation
You can find the full **Documentation** on the following [link](https://bitbucket.org/PGS_ART/module-gesture-recognition/src/dd2189a475150ae49fee2adcda1e4a1af25b216a/Assets/_HelpBox/Documentation.docx?at=master&fileviewer=file-view-default)



# Editor Settings

- **Enable Singleton:** It will allow you to keep one single game object over the game life cycle (Recomanded).

- **Enable Debug Mode:** It will allow you to show 3 different types of dot on your current scene describing

- **Blue Dot:** Showing the idle shape that you are trying to track.

- **Green Dot:**  Accepted dot by the shape.

- **Red Dot:** Discarded dot by the shape.

If ratio of green and red dot is greater than the targeted result ( overallSmoothness * smoothness – of shape);

- **Overall Smoothness:** It will overall set the smoothness of all the given shape by multiplying the 'smoothness' of every shape.

- **Shape:** Creates the number of shape that you want to track over the game life cycle.

- **Shape(X):** From the following panel, you will be able to configure your shape with the given parameter.

- **(X):**  The following 'X' denotes the index of the shape.

- **Smoothness:** Greater the smoothness, easier it is to find this specific shape.

- **Shape Matrix Size:** It will allow you to size/resize the matrix grid where you will draw your shape. The grid will automatically optimize when you 'Play' the game or run on physical device. You can draw the shape by simply 'Tick' mark the grid.

- **On Shape Found () :** The following event gets triggered when the user correctly draw the shape.

- **On Shape Search Begin () :** The following event gets triggered when the game start finding the shape from the user.

- **On Shape Search Failed () :** When user failed to draw the shape.

# Custom Function

**Overview:** If the following script is set as singleton, you will be able to call the public call back by **GestureRecognitionHandler.Instance.YourFunction**.

- **FindShape(int m_Index):** By passing the shape index, It will start finding that following shape until, it finds/fail.

